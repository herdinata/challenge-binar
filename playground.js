const playerRock = document.getElementById("playerRock");
const playerPaper = document.getElementById("playerPaper");
const playerScissors = document.getElementById("playerScissors");
const comRock = document.getElementById("comRock");
const comPaper = document.getElementById("comPaper");
const comScissors = document.getElementById("comScissors");
const output = document.getElementById("output");
const allImageRPS = document.querySelectorAll("img");

let comPilihan = "";
let playerScoreNumber = 0;
let comScoreNumber = 0;

class Pilihan {
  constructor(value, defeat) {
    this.value = value;
    this.defeat = defeat;
  }
}

class Rock extends Pilihan {
  constructor() {
    super("Rock", "Scissors");
  }
}

class Paper extends Pilihan {
  constructor() {
    super("Paper", "Rock");
  }
}

class Scissors extends Pilihan {
  constructor() {
    super("Scissors", "Paper");
  }
}
const rock = new Rock();
const paper = new Paper();
const scissors = new Scissors();

function select(playerPilihan) {
  resetPilihan();

  if (playerPilihan == rock.value) {
    playerRock.classList.add("selectedStyle");
  } else if (playerPilihan == paper.value) {
    playerPaper.classList.add("selectedStyle");
  } else {
    playerScissors.classList.add("selectedStyle");
  }

  comRandomPilihan();
  updateHasil(playerPilihan);
}

function comRandomPilihan() {
  const comSelectedNumber = Math.random();

  if (comSelectedNumber < 0.3) {
    comPilihan = paper.value;
  } else if (comSelectedNumber > 0.3 && comSelectedNumber <= 0.6) {
    comPilihan = rock.value;
  } else {
    comPilihan = scissors.value;
  }
  tampilanComPilihan(comPilihan);
}

function tampilanComPilihan(comPilihan) {
  if (comPilihan === rock.value) {
    comRock.classList.add("selectedStyle");
  } else if (comPilihan === paper.value) {
    comPaper.classList.add("selectedStyle");
  } else {
    comScissors.classList.add("selectedStyle");
  }
}

function updateHasil(playerPilihan) {
  const milih = pemilih[playerPilihan];
  if (playerPilihan == comPilihan) {
    output.textContent = "  DRAW  ";
    output.style.color = "white";
    output.style.backgroundColor = "#035B0C";
    output.style.transform = "rotate(-25deg)";
    output.style.padding = "35px";
  } else if (milih.defeat.indexOf(comPilihan) > -1) {
    output.textContent = "PLAYER 1 WIN";
    output.style.color = "white";
    output.style.backgroundColor = "#4C9654";
    output.style.transform = "rotate(-25deg)";
    output.style.padding = "35px";
  } else {
    output.textContent = "COM WIN";
    output.style.color = "white";
    output.style.backgroundColor = "#4C9654";
    output.style.transform = "rotate(-25deg)";
    output.style.padding = "35px";
  }
}

function resetPilihan() {
  allImageRPS.forEach((img) => {
    img.classList.remove("selectedStyle");
  });
}
